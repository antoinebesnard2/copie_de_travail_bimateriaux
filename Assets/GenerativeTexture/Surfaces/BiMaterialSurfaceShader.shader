﻿Shader "Surfaces/BiMaterialSurfaceShader"
{
	Properties
	{
		_AlgoTex("From Algo Texture", 2D) = "red" {}
		_NormalStrengthAlgo("Normal Strength Algo", Range(0,1)) = 0.9
		_Advantage("A Advantage On B", Range(0,1)) = 0.5
			_Threshold("Threshold", Range(0, 1)) = 0.1
			_Fading("Edge Smoothing", Range(0, 1)) = 0.2
			_NonLinearFactor("NonLinearFactor", Float) = 0
	[Space]
	[Space]
	[Space]
	[Space]
		_Occlusion0("Ambient Occlusion 0", 2D) = "black" {}
		_Albedo0("Base Color 0", 2D) = "defaulttexture" {}
		_Height0("Height 0", 2D) = "black" {}
		_Metallic0("Metallic 0", 2D) = "black" {}
		_Normal0("Normal 0", 2D) = "bump" {}
		_Smoothness0("Smoothness 0", Range(0,1)) = 0.5
		_NormalStrength0("Normal Strength 0", Float) = 1
		_HeightStrength0("Height Strength 0", Range(0,1)) = 0.5
		_OcclusionStrength0("Occlusion Strength 0", Range(0,1)) = 0.5
		_ColorTint0("Color Tint 0", Color) = (1.0,1.0,1.0,1.0)
	[Space]
	[Space]
	[Space]
	[Space]
		_Occlusion1("Ambient Occlusion 1", 2D) = "black" {}
		_Albedo1("Base Color 1", 2D) = "defaulttexture" {}
		_Height1("Height 1", 2D) = "black" {}
		_Metallic1("Metallic 1", 2D) = "black" {}
		_Normal1("Normal 1", 2D) = "bump" {}
		_Smoothness1("Smoothness 1", Range(0,1)) = 0.5
		_NormalStrength1("Normal Strength 1", Float) = 1
		_HeightStrength1("Height Strength 1", Range(0,1)) = 0.5
		_OcclusionStrength1("Occlusion Strength 1", Range(0,1)) = 0.5
		_ColorTint1("Color Tint 1", Color) = (1.0,1.0,1.0,1.0)
	[Space]
	[Space]
	[Space]
	[Space]
	[ToggleOff] _SpecularHighlights("Specular Highlights", Float) = 1.0
	[ToggleOff] _GlossyReflections("Glossy Reflections", Float) = 1.0

	}
		SubShader
	{
		Tags{ "RenderType" = "Opaque" }

		CGPROGRAM

#pragma surface surf Standard
#pragma target 3.0

	//	sampler2D _MainTex;

	struct Input { 
		float2 uv_AlgoTex,
			uv_Albedo0,uv_Albedo1;
	};

	float4 _AlgoTex_TexelSize;

	fixed4  _ColorTint0, _ColorTint1;

	sampler2D _AlgoTex,
		_Occlusion0, _Albedo0, _Height0, _Metallic0, _Normal0,
		_Occlusion1, _Albedo1, _Height1, _Metallic1, _Normal1;

	half _Advantage, _NormalStrengthAlgo, _Smoothness0, _NormalStrength0, _HeightStrength0, _OcclusionStrength0,
		_Smoothness1, _NormalStrength1, _HeightStrength1, _OcclusionStrength1;

	half _Threshold, _Fading, _NonLinearFactor;

	half _SpecularHighlights, _GlossyReflections;

	void surf(Input IN, inout SurfaceOutputStandard o)
	{
		float3 duv = float3(_AlgoTex_TexelSize.xy, 0);

		half v0 = tex2D(_AlgoTex, IN.uv_AlgoTex).y;
		half v1 = tex2D(_AlgoTex, IN.uv_AlgoTex - duv.xz).y;
		half v2 = tex2D(_AlgoTex, IN.uv_AlgoTex + duv.xz).y;
		half v3 = tex2D(_AlgoTex, IN.uv_AlgoTex - duv.zy).y;
		half v4 = tex2D(_AlgoTex, IN.uv_AlgoTex + duv.zy).y;

		float3 color = float3(tex2D(_AlgoTex, IN.uv_AlgoTex).r + tex2D(_Height0, IN.uv_Albedo0).r *_HeightStrength0, tex2D(_AlgoTex, IN.uv_AlgoTex).g + tex2D(_Height1, IN.uv_Albedo1).r * _HeightStrength1, 0);
		if (color.r * _Advantage > color.g *(1- _Advantage))
		{
			fixed3 normal = UnpackNormal(tex2D(_Normal0, IN.uv_Albedo0));
			normal.z = normal.z * (_NormalStrength0);
			o.Normal = normalize(normal);
		}
		else {
			fixed3 normal = UnpackNormal(tex2D(_Normal1, IN.uv_Albedo1));
			normal.z = normal.z * (_NormalStrength1);
			o.Normal = normalize(normal);
		}
		half p = smoothstep(_Threshold, _Threshold + _Fading, v0) * _NonLinearFactor;

		o.Occlusion = lerp(tex2D(_Occlusion0, IN.uv_Albedo0).rgb, tex2D(_Occlusion1, IN.uv_Albedo1).rgb, p);
		o.Albedo = lerp(tex2D(_Albedo0, IN.uv_Albedo0).rgb  * _ColorTint0, tex2D(_Albedo1, IN.uv_Albedo1).rgb  * _ColorTint1,p);
		o.Metallic = lerp(tex2D(_Metallic0, IN.uv_Albedo0).rgb, tex2D(_Metallic1, IN.uv_Albedo1).rgb, p);
		o.Smoothness = lerp(_Smoothness0, _Smoothness1, p);

		o.Normal += normalize(float3(v1 - v2, v3 - v4, 1 - _NormalStrengthAlgo));
	}

	ENDCG
	}
		FallBack "Diffuse"
}
