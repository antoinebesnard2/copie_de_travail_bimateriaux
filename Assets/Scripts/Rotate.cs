﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;

public class Rotate : MonoBehaviour {

    [Tooltip("Number of seconds for a 360° rotation. Negative values rotate counter-clockwise.")]
    public float Speed;

    public Transform Pivot;
	
	// Update is called once per frame
	void Update () {
        if(Speed == 0.0f) return;

        gameObject.transform.RotateAround(Pivot.position, Vector3.up, Time.deltaTime * Speed);
    }
}
