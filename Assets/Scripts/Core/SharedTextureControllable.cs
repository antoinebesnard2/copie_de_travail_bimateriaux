﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SharedTextureControllable : Controllable
{
    public SharedTexture MySharedTexture;

    //Because of spout :/
    public MrSceneManager SceneManager;

    [OSCProperty] public bool SpoutOutput;

    // Use this for initialization
    public override void Awake()
    {
        usePresets = true;
        base.Awake();
    }
	
    // Update is called once per frame
    public override void Update ()
    {
        if (SceneManager.loadingScene)
            MySharedTexture.enabled = false;
        else
            MySharedTexture.enabled = SpoutOutput;

        base.Update();
    }
}

