﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OSCControlExample : Controllable
{
    [OSCProperty]
    [Range(-100.0f, 100.0f)]
    public float Speed;

    public Texture2D debugTexture;
    public Texture2D realTexture;
    public Rotate rotateScrip;

    private bool isDebugTexture;

    public override void Awake()
    {
        usePanel = true;
        debug = false;
        base.Awake();
        Speed = 40.0f;
    }

    public override void Update()
    {
        base.Update();
        rotateScrip.Speed = Speed;
    }

    [OSCMethod]
    public void ToggleDebugTexture()
    {
        if (!isDebugTexture)
        {
            GetComponent<Renderer>().material.mainTexture = debugTexture;
            isDebugTexture = true;
        }
        else
        {
            GetComponent<Renderer>().material.mainTexture = realTexture;
            isDebugTexture = false;
        }
    }
}
