using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MrSceneManager : Controllable
{
    public List<GameObject> ObjectsToKeepBetweenScenes;
    public string sceneLayer;

    public int sceneCount { get; set; }

    public List<string> scenesName;
    [OSCProperty(TargetList = "scenesName", IncludeInPresets = false)] public string activeScene;

    public int currSceneIndex;

    public bool loadingScene { get; set; }
    public bool launchingIntro { get; set; }
    public bool launchingOutro { get; set; }
    public bool playingScene { get; set; }

    private MrScene currMrScene;
    private Dictionary<string, int> scenesInBuild;

    public override void Awake()
    {
        scenesName = new List<string>();
        scenesInBuild = new Dictionary<string, int>();

        usePanel = true;
        usePresets = false;
        base.Awake();
        DontDestroyOnLoad(this);
        foreach (var item in ObjectsToKeepBetweenScenes)
        {
            DontDestroyOnLoad(item);
        }

        for (int i = 1; i < SceneManager.sceneCountInBuildSettings; i++)   //Starts at 1 to remove Core which is 0
        {
            var sceneName =
                System.IO.Path.GetFileNameWithoutExtension(UnityEngine.SceneManagement.SceneUtility
                    .GetScenePathByBuildIndex(i));
            scenesName.Add(sceneName);
            scenesInBuild.Add(sceneName, i);
        }
    }

    // Use this for initialization
    void Start()
    {
        // get scene count
        sceneCount = UnityEngine.SceneManagement.SceneManager.sceneCountInBuildSettings - 1;


        // init variables
        loadingScene = false;
        launchingIntro = false;
        launchingOutro = false;
        playingScene = false;

        // load first scene
        currSceneIndex = 1;
        SceneManager.LoadScene(currSceneIndex);
        activeScene = SceneManager.GetSceneByBuildIndex(currSceneIndex).name;

        RaiseEventValueChanged("activeScene");
    }

    public override void Update()
    {
        if (activeScene != SceneManager.GetActiveScene().name && !loadingScene)
            LoadSceneWithName(activeScene);

        base.Update();
    }

    [OSCMethod]
    public void Next()
    {
        if(!loadingScene)
            StartCoroutine(loadScene(currSceneIndex + 1));
    }

    [OSCMethod]
    public void Previous()
    {
        if (!loadingScene)
            StartCoroutine(loadScene(currSceneIndex - 1));
    }

    [OSCMethod]
    public void LoadSceneWithName(string name)
    {
        if (!loadingScene)
        {
            Debug.Log("Build index : " + scenesInBuild[name] + " | Current index : " + currSceneIndex);
            loadingScene = true;
            StartCoroutine(loadScene(scenesInBuild[name]));
        }
    }

    public IEnumerator loadScene(int nextSceneIndex)
    {
        // a new scene is loading
        loadingScene = true;
        if (nextSceneIndex < 1) nextSceneIndex = 1;
        if (nextSceneIndex > sceneCount) nextSceneIndex = sceneCount;

        if (currMrScene != null)
        {
            // launch scene outro
            currMrScene.End();
            playingScene = false;
            launchingOutro = true;

            // wait appropriate amount of time
            yield return new WaitForSeconds(currMrScene.outroDuration);

            // scene outro is done
            launchingOutro = false;
        }

        currSceneIndex = nextSceneIndex;

        
        yield return new WaitForFixedUpdate(); //To allow SharedTextureControllable to disable spout;
        Debug.Log("Loading scene index " + currSceneIndex);
        UnityEngine.SceneManagement.SceneManager.LoadScene(currSceneIndex);
        activeScene = SceneManager.GetActiveScene().name;
        loadingScene = false; 
    }

    void OnEnable()
    {
        UnityEngine.SceneManagement.SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable()
    {
        UnityEngine.SceneManagement.SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        activeScene = SceneManager.GetActiveScene().name;
        // do not process Main scene
        if (scene.name == "Core") return;

        // fetch parent object of scene
        //GameObject go = scene.GetRootGameObjects()[0];

        bool foundMrScene = false;
        foreach (GameObject go in scene.GetRootGameObjects())
        {
            if (go.GetComponent<MrScene>() != null)
            {
                // get components in object and set variables
                currMrScene = go.GetComponent<MrScene>();
                currMrScene.rootGameObject = go;
                currMrScene.sceneIndex = currSceneIndex;

                foundMrScene = true;
            }
        }

        if (!foundMrScene)
        {
            Debug.LogWarning("Couldn't find MrScene component in scene.");
            return;
        }

        // log
        Debug.Log("Level " + scene.name + " loaded with id " + currMrScene.id + " and build index " + currSceneIndex);

        ControllableMaster.LoadEveryPresets();

        // scene has succesfully loaded
        loadingScene = false;

        // launch intro of loaded scene
        StartCoroutine(currMrScene.Launch());
        playingScene = false;
        launchingIntro = true;
    }
}
